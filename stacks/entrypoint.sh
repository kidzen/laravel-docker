#!/usr/bin/env /bin/bash
set -e

if [ "${1#-}" != "$1" ]; then
    # start php-fpm service daemon
	set -- php-fpm "$@"
else
    # php artisan optimize && \
    # php artisan migrate --force

    service nginx start
fi

exec "$@"
